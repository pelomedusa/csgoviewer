package view.Panel;
import model.MainModel;
import model.Match;

import javax.swing.*;
import java.awt.*;

/**
 * Created by pelomedusa on 13/01/16.
 */
public class MainPane extends JPanel{
    private MainModel model;
    private ListMatchesPane panoListMatches;
    private ScrollerPane panoScroll;
    private ExpandedPane panoExpanded = null;

    public MainPane(MainModel m){
        super();
        setLayout(null);
        this.model = m;
        //setLayout(new ScrollPaneLayout(FlowLayout.LEFT));
        setPreferredSize(new Dimension(model.WIDTH,model.HEIGHT));
        //setLayout(new FlowLayout(FlowLayout.LEFT));
        setBackground(model.DARKGREY);
        //setBorder(new LineBorder(model.ORANGE, 1))
        addComponents();
    }

    public void expand(Match match) {
        setPreferredSize(new Dimension(model.WIDTH+model.EXPANSION_WIDTH,model.HEIGHT));
        removeAll();
        add(panoScroll);
        panoExpanded = new ExpandedPane(model,match);
        add(panoExpanded);

    }

    public void retract() {
        setPreferredSize(new Dimension(model.WIDTH,model.HEIGHT));
        removeAll();
        add(panoScroll);
    }

    private void addComponents() {
        panoListMatches = new ListMatchesPane(model);
        panoScroll = new ScrollerPane(model, panoListMatches);
        this.add(panoScroll);
    }

    public MainModel getModel() {
        return model;
    }

    public void setModel(MainModel model) {
        this.model = model;
    }

    public ListMatchesPane getPanoListMatches() {
        return panoListMatches;
    }

    public void setPanoListMatches(ListMatchesPane panoListMatches) {
        this.panoListMatches = panoListMatches;
    }

    public ScrollerPane getPanoScroll() {
        return panoScroll;
    }

    public void setPanoScroll(ScrollerPane panoScroll) {
        this.panoScroll = panoScroll;
    }

    public ExpandedPane getPanoExpanded() {
        return panoExpanded;
    }

    public void setPanoExpanded(ExpandedPane panoExpanded) {
        this.panoExpanded = panoExpanded;
    }
}
