package view.Panel;
import model.MainModel;
import view.Button.ButtonMatch;
import model.Match;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by pelomedusa on 12/01/16.
 */
public class ListMatchesPane extends JPanel {
    private MainModel model;
    private ButtonMatch[] allButtonMatch;

    public ListMatchesPane(MainModel model){
        super();
        this.model = model;
        //setLayout(new ScrollPaneLayout(FlowLayout.LEFT));
        setPreferredSize(new Dimension(model.WIDTH_WITHOUT_SCROLLBAR,model.getNbrMatches()*model.REAL_HEIGHT_BUTTON_MATCH));

        //setLayout(new FlowLayout(FlowLayout.LEFT));
        setBackground(model.DARKGREY);
        //setBorder(new LineBorder(model.ORANGE, 1));

        addButtonMatches();
    }

    private void addButtonMatches() {
        System.out.println("creation buttons");
        allButtonMatch = new ButtonMatch[model.getNbrMatches()];
        ResultSet resultat = model.getAllIds();
        try {
            int j=0;
            while ( resultat.next() ) {
                Match match = new Match(resultat.getInt("idMatch"),model);
                allButtonMatch[j] = new ButtonMatch(model,match);
                add(allButtonMatch[j]);
                j++;


            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public MainModel getModel() {
        return model;
    }

    public void setModel(MainModel model) {
        this.model = model;
    }

    public ButtonMatch[] getAllButtonMatch() {
        return allButtonMatch;
    }

    public void setAllButtonMatch(ButtonMatch[] allButtonMatch) {
        this.allButtonMatch = allButtonMatch;
    }
}
