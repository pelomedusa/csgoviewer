package model;

import controller.Database;

import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by pelomedusa on 12/01/16.
 */



public class MainModel implements Constantes {
    private Database db;
    private Font font_cs;
    private Font font_open24display_little;
    private Font font_open24display_big;

    public MainModel(){
        db= new Database();
        setFonts();
    }

    private void setFonts() {
        try {
            font_cs = Font.createFont(Font.TRUETYPE_FONT, getClass().getResource("/utils/fonts/cs_regular.ttf").openStream());
            font_open24display_little = Font.createFont(Font.TRUETYPE_FONT, getClass().getResource("/utils/fonts/DS-DIGIB.TTF").openStream());
            font_open24display_big = Font.createFont(Font.TRUETYPE_FONT, getClass().getResource("/utils/fonts/DS-DIGIB.TTF").openStream());

            GraphicsEnvironment genv = GraphicsEnvironment.getLocalGraphicsEnvironment();
            genv.registerFont(font_cs);
            genv.registerFont(font_open24display_little);
            genv.registerFont(font_open24display_big);


// makesure to derive the size
            font_cs = font_cs.deriveFont(20f);
            font_open24display_little = font_open24display_little.deriveFont(20f);
            font_open24display_big = font_open24display_big.deriveFont(30f);

        } catch (FontFormatException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ResultSet getAllMatches(){

        try {
            Statement statement = db.getConnection().createStatement();
            ResultSet resultat = statement.executeQuery( "SELECT *  FROM game ORDER BY idUrl;" );
            while ( resultat.next() ) {
                String titre = resultat.getString("idUrl");
                System.out.println(titre);
            }
            return resultat;
/*            while ( resultat.next() ) {
                String titre = resultat.getString("titre");
                System.out.println(titre);
            }*/
        } catch (SQLException e) {
            e.printStackTrace();
            System.exit(1);
            return null;
        }
    }

    public int getNbrMatches(){
        try {
            Statement statement = db.getConnection().createStatement();
            ResultSet resultat = statement.executeQuery( "SELECT COUNT(idMatch) AS nbrmatch from game;");
            resultat.next();
            return resultat.getInt("nbrmatch");
/*            while ( resultat.next() ) {
                String titre = resultat.getString("titre");
                System.out.println(titre);
            }*/
        } catch (SQLException e) {
            e.printStackTrace();
            System.exit(1);
            return 0;

        }
    }

    public ResultSet getAllIds(){
        ResultSet resultat = null;
        try {
            Statement statement = db.getConnection().createStatement();
            resultat = statement.executeQuery( "SELECT idMatch from game;");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultat;
    }

    public ResultSet getMatchById(int idMatch) {
        ResultSet resultat = null;
        try {
            Statement statement = db.getConnection().createStatement();
            resultat = statement.executeQuery( "SELECT * from game WHERE idMatch="+idMatch+";");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultat;
    }



    public Database getDb() {
        return db;
    }

    public void setDb(Database db) {
        this.db = db;
    }

    public Font getFont_cs() {
        return font_cs;
    }

    public void setFont_cs(Font font_cs) {
        this.font_cs = font_cs;
    }

    public Font getFont_open24display_little() {
        return font_open24display_little;
    }

    public void setFont_open24display_little(Font font_open24display_little) {
        this.font_open24display_little = font_open24display_little;
    }

    public Font getFont_open24display_big() {
        return font_open24display_big;
    }

    public void setFont_open24display_big(Font font_open24display_big) {
        this.font_open24display_big = font_open24display_big;
    }
}
