package view;

import model.MainModel;
import model.Match;
import view.Panel.MainPane;
import view.Panel.ScrollerPane;
import view.Panel.ListMatchesPane;

import javax.swing.*;

/**
 * Created by pelomedusa on 12/01/16.
 */
public class MainWindow extends JFrame {

    private MainModel model;
    private MainPane panoMain;
    private ListMatchesPane panoListMatches;
    private ScrollerPane panoScroll;
    private boolean expanded = false;

    public MainWindow(MainModel m){
        super();
        model = m;
        initWindow();
        initComponents();
        setVisible(true);
    }

    private void initComponents() {
        panoMain = new MainPane(model);
        setContentPane(panoMain);
    }

    private void initWindow() {
        setLayout(null);
        this.setSize(model.WIDTH, model.HEIGHT);
        setLocationRelativeTo(null);
        //setIconImage(imgIcon);
        setTitle("CSGO Viewer");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);

    }

    public void expand(Match match) {
        expanded = true;
        this.setSize(model.WIDTH+model.EXPANSION_WIDTH, model.HEIGHT);
        this.panoMain.expand(match);
        this.setContentPane(panoMain);
    }

    public void retract() {
        expanded = false;
        this.panoMain.retract();
        this.setSize(model.WIDTH, model.HEIGHT);
        this.setContentPane(panoMain);
    }

    public MainModel getModel() {
        return model;
    }

    public void setModel(MainModel model) {
        this.model = model;
    }

    public ListMatchesPane getPanoListMatches() {
        return panoListMatches;
    }

    public void setPanoListMatches(ListMatchesPane panoListMatches) {
        this.panoListMatches = panoListMatches;
    }

    public ScrollerPane getPanoScroll() {
        return panoScroll;
    }

    public void setPanoScroll(ScrollerPane panoScroll) {
        this.panoScroll = panoScroll;
    }

    public MainPane getPanoMain() {
        return panoMain;
    }

    public void setPanoMain(MainPane panoMain) {
        this.panoMain = panoMain;
    }

    public boolean isExpanded() {
        return expanded;
    }

    public void setExpanded(boolean expanded) {
        this.expanded = expanded;
    }
}
