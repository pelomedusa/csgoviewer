package view.Panel;

import model.MainModel;
import view.Scrollbar.MyScrollbarUI;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;

/**
 * Created by pelomedusa on 12/01/16.
 */
public class ScrollerPane extends JScrollPane{

    MainModel model;

    public ScrollerPane(MainModel m,ListMatchesPane mp){
        super(mp);
        this.model = m;
        setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        getVerticalScrollBar().setUI(new MyScrollbarUI(model.GREY));
        setBorder(new LineBorder(model.ORANGE, 1));
        //setPreferredSize(new Dimension(model.WIDTH,model.HEIGHT));
        setBounds(0,0,model.WIDTH,model.HEIGHT_SCROLLERPANE);
        getVerticalScrollBar().setUnitIncrement(16);
    }


}
