package view.Panel;
import model.MainModel;
import model.Match;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by pelomedusa on 14/01/16.
 */
public class TeamPane extends JPanel {
    MainModel model;
    Match match;
    int numeroEquipe;

    JLabel img;
    JLabel labTeam;
    JLabel labOdds;



    public TeamPane(MainModel model, int numeroEquipe, Match match){
        super();
        System.out.println("CREE");

        this.model=model;
        this.numeroEquipe = numeroEquipe;
        this.match = match;

        setPreferredSize(new Dimension(model.WIDTH_TEAMPANE,model.HEIGHT_TEAMPANE));
        setBackground(new Color(55, 55, 55, 123));


        if (numeroEquipe==1){
            setBorder(new LineBorder(model.COLOR_TEAM1, 1));
            setBounds(model.POSITION_X_TEAMPANE1,model.POSITION_Y_TEAMPANE1,model.WIDTH_TEAMPANE,model.HEIGHT_TEAMPANE);
            //setBackground(new Color(model.COLOR_TEAM1.getRed(), model.COLOR_TEAM1.getGreen(), model.COLOR_TEAM1.getBlue(), 60));
        }
        else {
            setBorder(new LineBorder(model.COLOR_TEAM2, 1));
            setBounds(model.POSITION_X_TEAMPANE2,model.POSITION_Y_TEAMPANE2,model.WIDTH_TEAMPANE,model.HEIGHT_TEAMPANE);
            //setBackground(new Color(model.COLOR_TEAM2.getRed(), model.COLOR_TEAM2.getGreen(), model.COLOR_TEAM2.getBlue(), 60));

        }

        initComponents();
        addComponents();


    }

    private void addComponents() {
        add(img);
        add(labTeam);
        add(labOdds);
    }

    private void initComponents() {
        if (numeroEquipe == 1){
            Color c;
            int odd = Integer.parseInt(match.getOddsT1().replace("%",""));
            if (odd>50){
                c = Color.GREEN;
            }else if (odd==50) {
                c = Color.YELLOW;
            }else{
                c=Color.decode("#FE5D5E");
            }

            img = new JLabel(new ImageIcon(match.getUrlIcoTeam1()));
            img.setBorder(new LineBorder(model.COLOR_TEAM1, 2));
            img.setHorizontalAlignment(SwingConstants.CENTER);


            labTeam = new JLabel(match.getTeam1());
            labTeam.setPreferredSize(new Dimension(model.WIDTH_MATCHPANE,30));
            labTeam.setHorizontalAlignment(SwingConstants.CENTER);
            labTeam.setForeground(model.COLOR_TEAM1);


            labOdds = new JLabel(match.getOddsT1());
            labOdds.setPreferredSize(new Dimension(model.WIDTH_MATCHPANE,30));
            labOdds.setHorizontalAlignment(SwingConstants.CENTER);
            labOdds.setForeground(c);
            labOdds.setFont(model.getFont_open24display_big());


        } else {
            Color c;
            int odd = Integer.parseInt(match.getOddsT2().replace("%",""));
            if (odd>50){
                c = Color.GREEN;
            }else if (odd==50) {
                c = Color.YELLOW;
            }else{
                c=Color.decode("#FE5D5E");
            }

            img = new JLabel(new ImageIcon(match.getUrlIcoTeam2()));
                img.setBorder(new LineBorder(model.COLOR_TEAM2, 2));
                img.setHorizontalAlignment(SwingConstants.CENTER);

                labTeam = new JLabel(match.getTeam2());
                labTeam.setPreferredSize(new Dimension(model.WIDTH_MATCHPANE,30));
                labTeam.setHorizontalAlignment(SwingConstants.CENTER);
                labTeam.setForeground(model.COLOR_TEAM2);

                labOdds = new JLabel(match.getOddsT2());
                labOdds.setPreferredSize(new Dimension(model.WIDTH_MATCHPANE,30));
                labOdds.setHorizontalAlignment(SwingConstants.CENTER);
                labOdds.setForeground(c);
                labOdds.setFont(model.getFont_open24display_big());
        }
        if (this.img.getIcon().getIconHeight()<0){
            img.setText("Verifiez votre connection");
        }

    }

}
