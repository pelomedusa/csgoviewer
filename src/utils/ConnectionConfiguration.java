package utils;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * Created by pelomedusa on 13/01/16.
 */
public class ConnectionConfiguration {

    public static Connection getConnection(){
        Connection connection=null;

        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/csgoviewer","root","1006");
        } catch (Exception e){
            e.printStackTrace();
        }

        return connection;
    }
}
