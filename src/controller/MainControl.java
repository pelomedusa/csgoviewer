package controller;

import model.MainModel;
import view.MainWindow;

/**
 * Created by pelomedusa on 13/01/16.
 */
public class MainControl {
    protected static MainWindow actualView;
    protected static MainModel actualModel;
    protected static ControlButtonMatch cbm;
    //protected static ControlMenu cm;

    public static void initControl(MainModel model) {
        actualModel = model;
        actualView = new MainWindow(model);
        //cg = new ControlGame(actualModel, actualView);
        cbm = new ControlButtonMatch(actualModel, actualView);
        System.out.println(actualModel.getNbrMatches()+ "---" +actualView.getPanoMain().getPanoListMatches().getAllButtonMatch().length);

        setButtonMatchController();
    }

    public static void setButtonMatchController(){
        for (int i=0; i< actualModel.getNbrMatches();i++){
            if (actualView.getPanoMain().getPanoListMatches().getAllButtonMatch()[i]==null){
                System.out.println("bouton inexistant "+i);
            }
            actualView.getPanoMain().getPanoListMatches().getAllButtonMatch()[i].addActionListener(cbm);
        }
    }
}
