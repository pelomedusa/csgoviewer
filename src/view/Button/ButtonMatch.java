package view.Button;

import model.MainModel;
import model.Match;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;

/**
 * Created by pelomedusa on 12/01/16.
 */

public class ButtonMatch extends JButton{

    MainModel model;
    Match match;

    JLabel labTeam1;
    JLabel labTeam2;


    public ButtonMatch(MainModel m, Match match){
        super();
        setLayout(null);


        this.model = m;
        this.match=match;
        this.setPreferredSize(new Dimension(model.WIDTH_BUTTON_MATCH,model.HEIGHT_BUTTON_MATCH));
        this.setBorder(new LineBorder(model.LIGHTGREY, 3));
        this.setBackground(model.DARKGREY);

        labTeam1 = new JLabel(match.getTeam1());
        labTeam1.setBounds(20,model.HEIGHT_BUTTON_MATCH/3,80,model.HEIGHT_BUTTON_MATCH/3);
        //labTeam1.setFont(model.getFont_cs());
        labTeam1.setForeground(model.COLOR_TEAM1);

        labTeam2 = new JLabel(match.getTeam2());
        labTeam2.setBounds(model.WIDTH_BUTTON_MATCH-90,model.HEIGHT_BUTTON_MATCH/3,model.WIDTH_BUTTON_MATCH-20,model.HEIGHT_BUTTON_MATCH/3);
        //labTeam2.setFont(model.getFont_cs());
        labTeam2.setForeground(model.COLOR_TEAM2);


        add(labTeam1);
        add(labTeam2);

    }



    public Match getMatch() {
        return match;
    }

    public void setMatch(Match match) {
        this.match = match;
    }

    public JLabel getLabTeam1() {
        return labTeam1;
    }

    public void setLabTeam1(JLabel labTeam1) {
        this.labTeam1 = labTeam1;
    }

    public JLabel getLabTeam2() {
        return labTeam2;
    }

    public void setLabTeam2(JLabel labTeam2) {
        this.labTeam2 = labTeam2;
    }

    public void select() {
        this.setBorder(new LineBorder(Color.GREEN, 3));
    }

    public void unselect() {
        this.setBorder(new LineBorder(model.LIGHTGREY, 3));
    }
}
