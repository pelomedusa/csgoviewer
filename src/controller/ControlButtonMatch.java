package controller;
import model.Match;

import model.MainModel;
import view.Button.ButtonMatch;
import view.MainWindow;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by pelomedusa on 13/01/16.
 */
public class ControlButtonMatch implements ActionListener{
    MainModel model;
    MainWindow view;
    ButtonMatch currentlySelected = null;

    public ControlButtonMatch(MainModel m, MainWindow v){
        this.model =m;
        this.view = v;
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        //if ( (view.isExpanded()) && ((ButtonMatch) e.getSource()).getMatch().equals(view.getPanoMain().getE)){

        if ( (view.isExpanded()) && (((ButtonMatch) e.getSource()).getMatch().equals(view.getPanoMain().getPanoExpanded().getMatch()))){
            view.retract();
            ((ButtonMatch) e.getSource()).unselect();
            currentlySelected=null;

        }else {
            if (currentlySelected!=null) {
                currentlySelected.unselect();
            };
            view.expand(((ButtonMatch) e.getSource()).getMatch());
            currentlySelected = (ButtonMatch) e.getSource();
            currentlySelected.select();

        }
    }
}
