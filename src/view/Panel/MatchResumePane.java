package view.Panel;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.LineBorder;

import model.MainModel;
import model.Match;
import view.JFrame.CommandLauncher;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.Normalizer;

/**
 * Created by pelomedusa on 15/01/16.
 */
public class MatchResumePane extends JPanel{
    private Match match;
    private MainModel model;
    private JLabel labTime;
    private JLabel labDate;
    private JLabel labUrl;
    private JLabel labWinner = null;
    private JLabel labBo;
    private JLabel labLive = null;

    private JButton buttonWatch;
    private JComboBox<String> listQualities;


    public MatchResumePane(MainModel model, Match match){
        super();

        this.model=model;
        this.match = match;

        setPreferredSize(new Dimension(model.WIDTH_TEAMPANE,model.HEIGHT_TEAMPANE));
        //setBackground(model.GREY);
        setBackground(new Color(55, 55, 55, 123));
        setBounds(model.POSITION_X_MATCHPANE,model.POSITION_Y_MATCHPANE,model.WIDTH_MATCHPANE,model.HEIGHT_MATCHPANE);
       if (match.isLive()==true){
            setBorder(new LineBorder(Color.GREEN, 3));
        }
        /* else {
            setBorder(new LineBorder(model.VERYLIGHTGREY, 1));
        }*/

        initComponents();
        addComponents();


    }

    private void addComponents() {
        add(labDate);
        add(labTime);
        if (!(match.getWinner().contains("aucun"))){
            add(labWinner);
        }
        add(labBo);
        add(labUrl);
        if (buttonWatch!=null){
            add(listQualities);

            add(buttonWatch);
        }
    }

    private void initComponents() {
        //labLive = new JLabel(match.isLive());

        String[] heuredate = match.getDate().split(" 2016");
        labDate = new JLabel("<html><div style=\"text-align: center;\">" +heuredate[0]+ "</br></html>");
        labDate.setPreferredSize(new Dimension(model.WIDTH_MATCHPANE,30));
        labDate.setHorizontalAlignment(SwingConstants.CENTER);
        labDate.setForeground(Color.WHITE);
        labDate.setFont(model.getFont_cs());

        labTime = new JLabel(heuredate[1]);
        labTime.setPreferredSize(new Dimension(model.WIDTH_MATCHPANE,30));
        labTime.setHorizontalAlignment(SwingConstants.CENTER);
        labTime.setForeground(Color.WHITE);
        labTime.setFont(model.getFont_open24display_big());

        String winner = match.getWinner();
        labWinner = new JLabel("won by: "+winner);
        labWinner.setPreferredSize(new Dimension(model.WIDTH_MATCHPANE,30));
        labWinner.setHorizontalAlignment(SwingConstants.CENTER);
        labWinner.setForeground(Color.WHITE);
        labWinner.setFont(model.getFont_open24display_big());

        labBo = new JLabel(match.getBo());
        labBo.setPreferredSize(new Dimension(model.WIDTH_MATCHPANE,30));
        labBo.setHorizontalAlignment(SwingConstants.CENTER);
        labBo.setForeground(Color.WHITE);

        labUrl = new JLabel("http://csgolounge.com/match?m="+match.getIdUrl());
        labUrl.setPreferredSize(new Dimension(model.WIDTH_MATCHPANE,30));
        labUrl.setHorizontalAlignment(SwingConstants.CENTER);
        labUrl.setForeground(Color.ORANGE);

        if (match.isLive()){
            listQualities = new JComboBox<String>(new String[] {"mobile","low","medium","high","source"});

            buttonWatch = new JButton();
            buttonWatch.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    ((JButton) e.getSource()).setEnabled(false);
                    CommandLauncher cl = new CommandLauncher("livestreamer " +match.getUrlStream()+ "high",((JButton) e.getSource()));
                    Thread t = new Thread(cl);
                    t.start();

                }

            });
            buttonWatch.setPreferredSize(new Dimension(70,30));
            buttonWatch.setBorderPainted(false);
            buttonWatch.setContentAreaFilled(false);
            buttonWatch.setFocusPainted(false);
            buttonWatch.setOpaque(false);
            try {
                buttonWatch.setIcon(new ImageIcon(ImageIO.read(new File("src/utils/img/buttonWatch.png"))));
                buttonWatch.setPressedIcon(new ImageIcon(ImageIO.read(new File("src/utils/img/buttonWatchPressed.png"))));
                buttonWatch.setDisabledIcon(new ImageIcon(ImageIO.read(new File("src/utils/img/buttonWatchDisabled.png"))));
            } catch (IOException e) {
                e.printStackTrace();
            }
            //buttonWatch.setBorder(Color.BLACK);
        }

    }

    public JButton getButtonWatch() {
        return buttonWatch;
    }

    public void setButtonWatch(JButton buttonWatch) {
        this.buttonWatch = buttonWatch;
    }

    public JComboBox<String> getListQualities() {
        return listQualities;
    }

    public void setListQualities(JComboBox<String> listQualities) {
        this.listQualities = listQualities;
    }
}
