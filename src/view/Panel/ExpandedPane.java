package view.Panel;
import model.MainModel;
import model.Match;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.io.File;
import java.io.IOException;


/**
 * Created by pelomedusa on 13/01/16.
 */
public class ExpandedPane extends JPanel{

    MainModel model;
    Match match;

    public ExpandedPane(MainModel model, Match match){
        super();
        this.model = model;
        this.match = match;
        //setLayout(new ScrollPaneLayout(FlowLayout.LEFT));
        //setPreferredSize(new Dimension(model.EXPANSION_WIDTH,model.HEIGHT));
        //setLayout(new FlowLayout(FlowLayout.LEFT));
        //setBackground(model.GREY);
        //setBorder(new LineBorder(model.ORANGE, 1));
        setBorder(new LineBorder(model.ORANGE, 1));
        setBounds(model.WIDTH,0,model.EXPANSION_WIDTH,model.HEIGHT_EXPANDPANE);
        setLayout(null);
        add(new MatchResumePane(model, match));
        add(new TeamPane(model,1,match));
        add(new TeamPane(model,2,match));
        System.out.println(model.EXPANSION_WIDTH+"x"+model.HEIGHT_EXPANDPANE);


    }

    @Override
    protected void paintComponent(Graphics g){
        super.paintComponent(g);
        try {
            g.drawImage(ImageIO.read(new File("src/utils/img/background.png")), 0, 0, null);
        }
        catch (IOException e){System.out.println("Image wagon.png non trouvée.");System.exit(0);}
    }

    public MainModel getModel() {
        return model;
    }

    public void setModel(MainModel model) {
        this.model = model;
    }

    public Match getMatch() {
        return match;
    }

    public void setMatch(Match match) {
        this.match = match;
    }
}
