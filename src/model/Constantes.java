package model;

import java.awt.*;

/**
 * Created by pelomedusa on 12/01/16.
 */
public interface Constantes {

        public static final int WIDTH= 265; //car +15 pour scrollbar
        public static final int HEIGHT= 500;

        public static final int WIDTH_WITHOUT_SCROLLBAR= WIDTH-15; //car +15 pour scrollbar
        public static final int HEIGHT_WITHOUT_SCROLLBAR= HEIGHT;

        public static final int HEIGHT_SCROLLERPANE= HEIGHT-26; //car +15 pour scrollbar
        public static final int HEIGHT_EXPANDPANE= HEIGHT-26;
        public static final int EXPANSION_WIDTH = 500;

        public static final int WIDTH_BUTTON_MATCH = WIDTH*9/10;
        public static final int HEIGHT_BUTTON_MATCH = 73;
        public static final int REAL_HEIGHT_BUTTON_MATCH = HEIGHT_BUTTON_MATCH+5;


        public static final int POSITION_X_TEAMPANE1 = 10;
        public static final int POSITION_Y_TEAMPANE1 = HEIGHT_EXPANDPANE/2+10;

        public static final int POSITION_X_TEAMPANE2 = EXPANSION_WIDTH/2+10;
        public static final int POSITION_Y_TEAMPANE2 = HEIGHT_EXPANDPANE/2+10;

        public static final int WIDTH_TEAMPANE = EXPANSION_WIDTH/2-20;
        public static final int HEIGHT_TEAMPANE = HEIGHT_EXPANDPANE/2-20;

        public static final int POSITION_X_MATCHPANE = 10;
        public static final int POSITION_Y_MATCHPANE = 10;

        public static final int WIDTH_MATCHPANE = EXPANSION_WIDTH-20;
        public static final int HEIGHT_MATCHPANE = HEIGHT_EXPANDPANE/2-20;

        /*
        public static final int POSITION_X_TEAMPANE1 = 10;
        public static final int POSITION_Y_TEAMPANE1 = 10;

        public static final int POSITION_X_TEAMPANE2 = 10;
        public static final int POSITION_Y_TEAMPANE2 = HEIGHT_EXPANDPANE/2+10;

        public static final int WIDTH_TEAMPANE = EXPANSION_WIDTH-20;
        public static final int HEIGHT_TEAMPANE = HEIGHT_EXPANDPANE/2-20;

        *
        * */



        public static final int NBR_MATCH = 15;


        public static final Color VERYLIGHTGREY = Color.decode("#C5C5C5");
        public static final Color LIGHTGREY = Color.decode("#595C5E");
        public static final Color GREY = Color.decode("#3C3F41");
        public static final Color DARKGREY = Color.decode("#272822");
        public static final Color ORANGE = Color.decode("#E4560E");

        //public static final Color COLOR_TEAM1 = Color.GREEN;
        //public static final Color COLOR_TEAM2 = Color.CYAN;

        //public static final Color COLOR_TEAM1 = Color.ORANGE;
        //public static final Color COLOR_TEAM2 = Color.decode("#1D04D1");

        public static final Color COLOR_TEAM1 = Color.ORANGE;
        public static final Color COLOR_TEAM2 = Color.MAGENTA;






}
