package view.JFrame;

import view.Panel.MainPane;

import javax.swing.*;
import java.awt.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Created by pelomedusa on 18/01/16.
 */
public class CommandLauncher extends JFrame implements Runnable{
    private JButton btn;
    private String command;
    private boolean isRunning = true;

    public CommandLauncher(String command, JButton btn){
        this.command=command;
        this.btn=btn;
        initWindow();
        initComponents();
        setVisible(true);

    }

    public void run() {
        executeCommand(command);
        System.out.println("commande finie");
        setVisible(false);
        dispose();
        btn.setEnabled(true);

    }

    public String executeCommand(String command) {

        StringBuffer output = new StringBuffer();
        Process p;
        try {
            p = Runtime.getRuntime().exec(command);
            p.waitFor();
            BufferedReader reader =
                    new BufferedReader(new InputStreamReader(p.getInputStream()));

            String line = "";
            while ((line = reader.readLine())!= null) {
                output.append(line + "\n");
                System.out.println(line);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return output.toString();

    }

    private void initComponents() {
        JPanel panoMain = new JPanel();
        panoMain.setBackground(Color.LIGHT_GRAY);
        setContentPane(panoMain);
    }

    private void initWindow() {
        setSize(300, 200);
        setLocationRelativeTo(null);
        //setIconImage(imgIcon);
        setTitle("Launching livestreamer ...");
        setResizable(false);

    }

    public boolean isRunning() {
        return isRunning;
    }

    public void setRunning(boolean running) {
        isRunning = running;
    }
}
