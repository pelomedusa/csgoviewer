package model;

import model.MainModel;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by pelomedusa on 13/01/16.
 */
public class Match {
    int idMatch;
    MainModel model;

    private int idUrl;
    private String team1 = "";
    private String team2 = "";
    private String oddsT1;
    private String oddsT2;
    private String winner;
    private String bo;
    private String date;
    private boolean live  = false;
    private URL urlIcoTeam1 = null;
    private URL urlIcoTeam2 = null;
    private String urlStream;

    public Match(int idMatch, MainModel model){
        this.model=model;
        this.idMatch=idMatch;
        ResultSet rs = this.model.getMatchById(this.idMatch);
        try {
            rs.next();

            this.idUrl= rs.getInt("idUrl");
            this.team1= rs.getString("team1");
            this.team2= rs.getString("team2");
            this.oddsT1= rs.getString("oddsT1");
            this.oddsT2= rs.getString("oddsT2");
            this.winner= rs.getString("winner");
            this.bo= rs.getString("bo");
            this.date= rs.getString("heure");
            this.urlStream=rs.getString("urlStream");

            if (this.live= rs.getInt("live")==1){
                this.live=true;
            }else{
                this.live=false;
            }


            this.urlIcoTeam1= new URL(rs.getString("urlIcoTeam1"));
            this.urlIcoTeam2= new URL(rs.getString("urlIcoTeam2"));
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }


    }

    public int getIdMatch() {
        return idMatch;
    }

    public void setIdMatch(int idMatch) {
        this.idMatch = idMatch;
    }

    public MainModel getModel() {
        return model;
    }

    public void setModel(MainModel model) {
        this.model = model;
    }

    public int getIdUrl() {
        return idUrl;
    }

    public void setIdUrl(int idUrl) {
        this.idUrl = idUrl;
    }

    public String getTeam1() {
        return team1;
    }

    public void setTeam1(String team1) {
        this.team1 = team1;
    }

    public String getTeam2() {
        return team2;
    }

    public void setTeam2(String team2) {
        this.team2 = team2;
    }

    public String getOddsT1() {
        return oddsT1;
    }

    public void setOddsT1(String oddsT1) {
        this.oddsT1 = oddsT1;
    }

    public String getOddsT2() {
        return oddsT2;
    }

    public void setOddsT2(String oddsT2) {
        this.oddsT2 = oddsT2;
    }

    public String getWinner() {
        return winner;
    }

    public void setWinner(String winner) {
        this.winner = winner;
    }

    public String getBo() {
        return bo;
    }

    public void setBo(String bo) {
        this.bo = bo;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public boolean isLive() {
        return live;
    }

    public void setLive(boolean live) {
        this.live = live;
    }

    public URL getUrlIcoTeam1() {
        return urlIcoTeam1;
    }

    public void setUrlIcoTeam1(URL urlIcoTeam1) {
        this.urlIcoTeam1 = urlIcoTeam1;
    }

    public URL getUrlIcoTeam2() {
        return urlIcoTeam2;
    }

    public void setUrlIcoTeam2(URL urlIcoTeam2) {
        this.urlIcoTeam2 = urlIcoTeam2;
    }

    public String getUrlStream() {return urlStream;}

    public void setUrlStream(String urlStream) {this.urlStream = urlStream;}
}
