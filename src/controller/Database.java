package controller;

import utils.ConnectionConfiguration;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by pelomedusa on 13/01/16.
 */
public class Database {
    Connection connection = null;

    public Database(){
        connecter();

    }
    public void connecter(){
        try {
            connection = ConnectionConfiguration.getConnection();
            if (connection!=null) {
                System.out.println("connection established!");
            }else{

            }
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    public void deconnecter(){
        try {
            connection.close();
            System.out.println("connection closed!");
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }
}
